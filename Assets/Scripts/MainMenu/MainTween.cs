﻿using UnityEngine;
using System.Collections;

public class MainTween : MonoBehaviour
{
    public GameObject targetPos;

    public void OnEnable()
    {
        iTween.MoveTo(gameObject, iTween.Hash("position", targetPos.transform.position, "time", 4, "easetype", iTween.EaseType.easeInOutCubic));
        iTween.RotateTo(gameObject, iTween.Hash("rotation", targetPos.transform.localEulerAngles, "time", 4, "easetype", iTween.EaseType.easeInOutCubic, "oncomplete", "DisableMain", "oncompletetarget", gameObject)); 
    }

    public void DisableMain()
    {
        GameObject.Find("MainCamera").GetComponent<CameraControl>().isChangingScreen = false;
        GetComponent<MainTween>().enabled = false;
    }
}