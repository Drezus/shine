﻿using UnityEngine;
using System.Collections;

public class FadeTween : MonoBehaviour
{
    public string StageName;

    public void Start()
    {
        iTween.FadeFrom(gameObject, iTween.Hash("a", 1, "delay", 1, "time", 4f));
    }

    public void FadeOut()
    {
        iTween.FadeTo(gameObject, iTween.Hash("a", 1, "time", 1.5f, "oncomplete", "ChangeStage", "oncompletetarget", gameObject));
    }

    public void ChangeStage()
    {
        switch (StageName)
        {
            case "TimesIntro":
                Application.LoadLevel("TimesIntro");
                break;
            default:
                Application.LoadLevel("Main Menu");
                break;
        }
    }
}