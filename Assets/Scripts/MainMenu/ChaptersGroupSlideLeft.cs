﻿using UnityEngine;
using System.Collections;

public class ChaptersGroupSlideLeft : MonoBehaviour
{
    public void OnEnable()
    {
            GameObject.Find("MainCamera").GetComponent<CameraControl>().chapterIndex++;
            iTween.MoveBy(gameObject, iTween.Hash("x", 17, "time", 1, "easetype", iTween.EaseType.easeInOutCubic, "oncomplete", "DisableSlideLeft", "oncompletetarget", gameObject));
    }

    public void DisableSlideLeft()
    {
        GameObject.Find("MainCamera").GetComponent<CameraControl>().isChangingScreen = false;
        GetComponent<ChaptersGroupSlideLeft>().enabled = false;
    }
}