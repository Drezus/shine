﻿using UnityEngine;
using System.Collections;

public class ChaptersTween : MonoBehaviour
{
    public GameObject targetPos;

    public void OnEnable()
    {
        iTween.MoveTo(gameObject, iTween.Hash("position", targetPos.transform.position, "time", 4, "easetype", iTween.EaseType.easeInOutCubic));
        iTween.RotateTo(gameObject, iTween.Hash("rotation", targetPos.transform.localEulerAngles, "time", 4, "easetype", iTween.EaseType.easeInOutCubic, "oncomplete", "DisableChapters", "oncompletetarget", gameObject)); 
    }

    public void DisableChapters()
    {
        GameObject.Find("MainCamera").GetComponent<CameraControl>().isChangingScreen = false;
        GetComponent<ChaptersTween>().enabled = false;
    }
}