﻿using UnityEngine;
using System.Collections;

public class OpeningCamera : MonoBehaviour
{
    public MovieTexture introVideo;

    public void Start()
    {
        Screen.lockCursor = true;
        introVideo.Play();
        introVideo.loop = false;
        Invoke("ChangeScreen", 11);
    }

    void ChangeScreen()
    {
        Application.LoadLevel("Main Menu");
    }
       
}