﻿using UnityEngine;
using System.Collections;

public class MenuButtonTween : MonoBehaviour
{
    public string TargetScreen;
    public GameObject mainCam;
    public bool isSelected = false;
    public bool isChangingScreen = false;
    private Color mainColor;
    public Color flashColor;

    public void Start()
    {
        mainColor = gameObject.renderer.material.color;
    }

    public void Update()
    {
        if (isSelected)
        {
            iTween.ColorTo(gameObject, iTween.Hash("color", flashColor, "time", 0.2f, "easetype", iTween.EaseType.easeInOutCubic));
            iTween.ColorTo(gameObject, iTween.Hash("color", mainColor, "time", 0.05f, "delay", 0.3f, "easetype", iTween.EaseType.easeInOutCubic));
        }
        isSelected = false;
    }
	
	
    public void ChangeScreen()
    {
        if (!GameObject.Find("MainCamera").GetComponent<CameraControl>().isChangingScreen)
        {
            GameObject.Find("MainCamera").GetComponent<CameraControl>().isChangingScreen = true;

            switch (TargetScreen)
            {
                
                case "Chapters":
                    mainCam.GetComponent<ChaptersTween>().enabled = true;
                    break;
                case "Credits":
                    mainCam.GetComponent<CreditsTween>().enabled = true;
                    break;
				
				case "Options":
                    mainCam.GetComponent<OptionsTween>().enabled = true;
                    break;


				case "qLow":
                    QualitySettings.SetQualityLevel(0);
                    GameObject.Find("MainCamera").GetComponent<CameraControl>().isChangingScreen = false;
                    break;
				case "qMedium":
                    QualitySettings.SetQualityLevel(1);
                    GameObject.Find("MainCamera").GetComponent<CameraControl>().isChangingScreen = false;
                    break;
				case "qHigh":
                    QualitySettings.SetQualityLevel(2);
                    GameObject.Find("MainCamera").GetComponent<CameraControl>().isChangingScreen = false;
                    break;
				case "qUltra":
                    QualitySettings.SetQualityLevel(3);
                    GameObject.Find("MainCamera").GetComponent<CameraControl>().isChangingScreen = false;
                    break;
				case "W14x9":
                    Screen.SetResolution(1280,720, false);
                    GameObject.Find("MainCamera").GetComponent<CameraControl>().isChangingScreen = false;
                    break;
				case "F14x9":
                    Screen.SetResolution(1440,900, true);
                    GameObject.Find("MainCamera").GetComponent<CameraControl>().isChangingScreen = false;
                    break;
				case "F16x9":
                    Screen.SetResolution(1280, 720, true);
                    GameObject.Find("MainCamera").GetComponent<CameraControl>().isChangingScreen = false;
                    break;
				case "F19x10":
                    Screen.SetResolution(1920,1080, true);
                    GameObject.Find("MainCamera").GetComponent<CameraControl>().isChangingScreen = false;
                    break;
				
                case "Logo":
                        mainCam.GetComponent<LogoTween>().enabled = true;
                        mainCam.GetComponent<CameraControl>().isOnLogo = true;
                    break;
                case "MainMenu":
                    mainCam.GetComponent<MainTween>().enabled = true;
                    break;
                case "ChaptersLeft":
                        GameObject.Find("ChaptersGroup").GetComponent<ChaptersGroupSlideRight>().enabled = true;
                    break;
                case "ChaptersRight":
                        GameObject.Find("ChaptersGroup").GetComponent<ChaptersGroupSlideLeft>().enabled = true;
                    break;



                case "TimesSquare":
                    GameObject.Find("FadePlane").GetComponent<FadeTween>().FadeOut();
                    GameObject.Find("FadePlane").GetComponent<FadeTween>().StageName = "TimesIntro";
                    mainCam.GetComponent<AudioSource>().mute = true;
                    break;
                case "AlphaTest":
                    GameObject.Find("FadePlane").GetComponent<FadeTween>().FadeOut();
                    GameObject.Find("FadePlane").GetComponent<FadeTween>().StageName = "AlphaTest";
                    mainCam.GetComponent<AudioSource>().mute = true;
                    break;


           
                default:
                    Application.LoadLevel("Main Menu");
                    break;

              }

            
        }
    }
}