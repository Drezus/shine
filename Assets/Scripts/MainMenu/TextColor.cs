﻿using UnityEngine;
using System.Collections;

public class TextColor : MonoBehaviour
{
    public Material color;
    void Awake()
    {
        transform.GetComponent<MeshRenderer>().material.SetColor("_Color", color.color);
    }
}