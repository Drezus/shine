﻿using UnityEngine;
using System.Collections;

public class ChaptersGroupSlideRight : MonoBehaviour
{
    public void OnEnable()
    {
            GameObject.Find("MainCamera").GetComponent<CameraControl>().chapterIndex--;
            iTween.MoveBy(gameObject, iTween.Hash("x", -17, "time", 1, "easetype", iTween.EaseType.easeInOutCubic, "oncomplete", "DisableSlideRight", "oncompletetarget", gameObject));
    }

    public void DisableSlideRight()
    {
        GameObject.Find("MainCamera").GetComponent<CameraControl>().isChangingScreen = false;
        GetComponent<ChaptersGroupSlideRight>().enabled = false;
    }
}