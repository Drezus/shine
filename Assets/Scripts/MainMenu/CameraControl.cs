﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour
{
    public bool isOnLogo = true;
    public bool isChangingScreen = false;
    public bool unlockCheat = false;
    public int chapterIndex = 1;
    public static int lockIndex = 1;
    public GameObject leftArrow;
    public GameObject rightArrow;
    public GameObject lockPanel;
    public Texture2D cursorTexture;

    public void Start()
    {
        Screen.lockCursor = false;
        Cursor.SetCursor(cursorTexture, Vector2.zero, CursorMode.Auto);
    }

    public void Update()
    {
        RaycastHit hitInfo;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo))
        {
            if (hitInfo.collider.tag == "menuOption")
            {
                hitInfo.collider.GetComponent<MenuButtonTween>().isSelected = true;

                if (Input.GetMouseButtonDown(0))
                {
                    hitInfo.collider.GetComponent<MenuButtonTween>().ChangeScreen();
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Return))
        {
            if (isOnLogo)
            {
                GetComponent<MainTween>().enabled = true;
                isChangingScreen = true;
                isOnLogo = false;
            }  
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        if (chapterIndex <= 1)
        {
            leftArrow.SetActive(false);
        }
        else
        {
            leftArrow.SetActive(true);
        }

        if (chapterIndex >= 10)
        {
            rightArrow.SetActive(false);
        }
        else
        {
            rightArrow.SetActive(true);
        }

        if (chapterIndex > lockIndex)
        {
            Invoke("LockScreen", 0.5f);
        }
        else
        {
            Invoke("UnlockScreen", 0.5f); 
        }

        //Unlock Cheat
        if (Input.GetKeyDown(KeyCode.F5))
        {
            switchCheatLockState();
        }

    }


    public void LockScreen()
    {
        if(!unlockCheat)
        lockPanel.SetActive(true);
    }

    public void UnlockScreen()
    {
        lockPanel.SetActive(false);
    }

    public void switchCheatLockState()
    {
        if (!unlockCheat) { unlockCheat = true; UnlockScreen(); }
        else if (unlockCheat) { unlockCheat = false; LockScreen(); }
    }
}