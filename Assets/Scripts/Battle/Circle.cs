﻿using UnityEngine;
using System.Collections;

public class Circle : MonoBehaviour
{
    private Transform target;
    private Vector2 randomVec;
    public GameObject[] aCircles;

    void Start()
    {
        target = GameObject.FindWithTag("Player").GetComponent<Focus>().target;

        RandomSpawn();
    }

    void OnMouseDown()
    {
        target.GetComponent<GlitchAI>().SendMessage("TakeDamage", GameObject.FindWithTag("Player").GetComponent<Focus>().damBase);
        Destroy(gameObject);
    }

    void RandomSpawn()
    {
        float randomX = UnityEngine.Random.Range(1, 94);
        randomX *= 0.01f;

        float randomY = UnityEngine.Random.Range(1, 85);
        randomY *= 0.01f;

        randomVec = new Vector2(randomX, randomY);
        transform.position = randomVec;

        aCircles = GameObject.FindGameObjectsWithTag("circle");

        foreach (GameObject circle in aCircles)
        {
            if (gameObject.GetComponent<GUITexture>().HitTest(circle.GetComponent<GUITexture>().transform.position))
            {
                RandomSpawn();
            }
            else
            {
                break;
            }
        }

        return;
    }
}