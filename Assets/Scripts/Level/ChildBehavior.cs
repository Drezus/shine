using UnityEngine;
using System.Collections;

public class ChildBehavior : MonoBehaviour {

    public float childInteraction;
    float abbyDistance;
    bool showGUI;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        transform.LookAt(GameObject.Find("MainCamera").transform.position);
        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);

        abbyDistance = Vector3.Distance(transform.position, GameObject.Find("Abby").transform.position);
        if (childInteraction > abbyDistance)
        {
            showGUI = true;
            animation.Stop();
        }
        else 
        {
            showGUI = false;
            animation.Play();
        }
	}

    void OnGUI() {
        if (showGUI)
        {
            if (GameObject.Find("EventBoxPeopleWalk01").GetComponent<EventControl>().hasKey == false)
            {
                GUI.Box(new Rect(Screen.width / 2 - 150, Screen.height - 50, 300, 20), "Voc� me ajuda a encontrar meu pai?");
            }
            else {
                GUI.Box(new Rect(Screen.width / 2 - 150, Screen.height - 50, 300, 20), "Obrigado! Agora sei onde encontrar ele!");
            }
        }
        else { }
    }
}
