using UnityEngine;
using System.Collections;

public class EffectsControl : MonoBehaviour {
	
	public float bloomIntensity;
	public Color fogEffectColor;
    public float mainT = 0;
    public Color colorCorruption;
    public Color colorChaos;
    public Color colorReal;
	
	void Start () {

	}
	
	void Update () {
		RenderSettings.fogColor = fogEffectColor;
        GameObject.Find("MainCamera").GetComponent<Bloom>().bloomIntensity = bloomIntensity;
	}
}
