using UnityEngine;
using System.Collections;

public class CarBehavScript : MonoBehaviour {

    public bool startStopping;
    public float distanceToFinish;
    public float timeToFinish;

	void OnTriggerEnter (Collider hit) {
        if (startStopping)
        {
            if (hit.collider.name == "Car7thAve(Clone)")
            {
                hit.collider.GetComponent<CarZAnim>().shouldItStop = true;
                iTween.MoveTo(hit.collider.gameObject, iTween.Hash("z", hit.collider.transform.position.z + distanceToFinish, "time", timeToFinish, "easetype", iTween.EaseType.easeOutQuad));
            }
        }
    }
}
