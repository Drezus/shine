﻿using UnityEngine;
using System.Collections;

public class SoundStop: MonoBehaviour
{
    private Transform Abby;

    public void Start()
    {
        Abby = GameObject.FindWithTag("Player").transform;
    }

    public void Update()
    {

        if (Abby.GetComponent<AbbyLife>().isDead || Abby.GetComponent<Focus>().inFocus)
        {
            if (GetComponent<AudioSource>())
            {
                GetComponent<AudioSource>().mute = true;
            }
        }
        else if (!Abby.GetComponent<AbbyLife>().isDead && !Abby.GetComponent<Focus>().inFocus)
        {
            if (GetComponent<AudioSource>())
            {
                GetComponent<AudioSource>().mute = false;
            }
        }
    }
}