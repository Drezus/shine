using UnityEngine;
using System.Collections;

public class PlayerDetection : MonoBehaviour {

    public GameObject platformB;

    void Start() {
        Respawn();
    }

    void OnTriggerEnter(Collider hit) {
            transform.FindChild("PlatformB(Clone)").GetComponent<DestroyEvent>().startAnim = true;
            Invoke("Respawn", 5);
    }

    void Respawn() {
        GameObject platformB2;
        platformB2 = GameObject.Instantiate(platformB, transform.position, transform.rotation) as GameObject;
        platformB2.transform.parent = transform;
    }
}
