using UnityEngine;
using System.Collections;

public class checkpointHud : MonoBehaviour {

    public string checkpointText;
    public GUIStyle checkpointGUI;
    bool showText = false;

    void OnTriggerEnter(Collider hit) {
        if (hit.collider.name == "Abby") {
            showText = true;
        }
    }

    void OnGUI() {
        if (showText) { GUI.Label(new Rect(30, 30, 200, 30), checkpointText, checkpointGUI); }
        else { }
    }

    void Update() {
        if (showText) { Invoke("HideText", 3); }
        else { }
    }

    void HideText() {
        showText = false;
    }
}
