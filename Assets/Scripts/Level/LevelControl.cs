using UnityEngine;
using System.Collections;

public class LevelControl : MonoBehaviour {

    public GameObject levelPP;
    public GameObject levelPA;
    public GameObject levelAP;
    public GameObject levelAA;
    public Transform levelPos;

    GameObject chosenLevel;
    public int actionLevel;
    public int platLevel;
    public bool isAction = false;

    public bool stop;

    void Update() {
        if (actionLevel > platLevel) //Ver se o jogador foi mais em a��o que plat
        {
            if (isAction) //verifica qual foi o primeiro
            {
                chosenLevel = levelAA;
            }
            else
            {
                chosenLevel = levelAP;
            }
        }
        else {
            if (isAction)
            {
                chosenLevel = levelPA;
            }
            else 
            {
                chosenLevel = levelPP;
            }
        }
    }

    void OnTriggerEnter() {
        if (!stop)
        {
            GameObject level;
            level = Instantiate(chosenLevel, levelPos.position, levelPos.rotation) as GameObject;
            stop = true;
        }
        else { }
    }
}
