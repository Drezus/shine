using UnityEngine;
using System.Collections;

public class PlatMobileSpawner : MonoBehaviour {

    public GameObject platformA;
    public bool doRespawn = false;

	void Start () {
        Respawn();
	}
	
	void Update () {
        if (doRespawn)
        {
            //Respawn();
            Invoke("Respawn", 5);
            doRespawn = false;
        }
        else { }
	}

    void Respawn() {
        GameObject platformA2;
        platformA2 = Instantiate(platformA, transform.position, transform.rotation) as GameObject;
        platformA2.transform.parent = transform;
    }
}
