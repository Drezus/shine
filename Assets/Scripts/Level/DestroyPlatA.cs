using UnityEngine;
using System.Collections;

public class DestroyPlatA : MonoBehaviour {

    bool detectOnce = false;

    void OnTriggerEnter(Collider hit) {
        if (!detectOnce)
        {
            if (hit.collider.tag == "Player")
            {
                //Invoke("DoRespawn", 2,1f);
                DoRespawn();
                transform.FindChild("PlatAnim").GetComponent<DestroyEvent>().startAnim = true;
                Destroy(transform.FindChild("PlatBase").gameObject, 2.1f);
                Destroy(gameObject, 2.1f);
                detectOnce = true;
            }
        }
    }

    void DoRespawn() {
        transform.parent.GetComponent<PlatMobileSpawner>().doRespawn = true;
    }

}
