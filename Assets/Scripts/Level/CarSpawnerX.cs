using UnityEngine;
using System.Collections;

public class CarSpawnerX : MonoBehaviour {

    public GameObject cars;
    public Transform spawnPos;
    bool stopInst = false;

    void OnTriggerEnter(Collider hit) {
        if (stopInst == false)
        {
            if (hit.collider.name == "Abby")
            {
                Instantiate(cars, spawnPos.position, spawnPos.rotation);
                stopInst = true;
            }
        }
        else { }
    }
}
