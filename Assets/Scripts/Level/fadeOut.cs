using UnityEngine;
using System.Collections;

public class fadeOut : MonoBehaviour {

    public float alpha;
    public GameObject vulto;
    private bool playOnce = false;

	void Start () {
	
	}
	
	void Update () {
        if(name != "NPCWall")transform.LookAt(GameObject.Find("MainCamera").transform.position);
        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);

        if (GameObject.Find("EventBoxPeopleWalk01").GetComponent<EventControl>().fadePeople == true)
        {
            renderer.material.color = new Color(renderer.material.color.r, renderer.material.color.g, renderer.material.color.b, alpha);
            if (!playOnce)
            {
                vulto.animation.Play();
                playOnce = true;
            }
            alpha -= Time.deltaTime / 2;
            Invoke("ObjDestroy", 4);
        }
	}

    void ObjDestroy() {
        Destroy(gameObject);
    }
}
