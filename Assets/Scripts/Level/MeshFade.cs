using UnityEngine;
using System.Collections;

public class MeshFade : MonoBehaviour {

    float alpha;
    bool startFade;
    float t = 0;

	void Start () {
        Invoke("FadeMaterial", 6);
	}

    void FadeMaterial() {
        startFade = true;
        
    }

    void Update() {
        Mathf.Clamp(t, 0, 1.01f);
        if (startFade) t += 0.3f * Time.deltaTime;
        renderer.material.color = new Color(renderer.material.color.r, renderer.material.color.g, renderer.material.color.b, alpha);
        alpha = Mathf.Lerp(1, 0, t);
        
    }
}
