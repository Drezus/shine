using UnityEngine;
using System.Collections;

public class CollectibleScript : MonoBehaviour {

    public GameObject eventBox;
    public AudioClip keySound;

    void OnTriggerEnter(Collider hit) {
        if (hit.collider.name == "Abby") {
            eventBox.GetComponent<EventControl>().keyNumber++;
            GameObject.Find("Music").GetComponent<AudioSource>().PlayOneShot(keySound);
            Destroy(gameObject);
        }
    }
}
