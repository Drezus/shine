using UnityEngine;
using System.Collections;

public class LevelCounter : MonoBehaviour {

    public bool isFull;
    public bool isCaos;
    public bool isFirst;

    int points;
    
    void Update () {
        if (isFull)
        {
            points = 2;
        }
        else {
            points = 1;
        }
	}

    void OnTriggerEnter() {
        if (isCaos)
        {
            GameObject.Find("LevelController").GetComponent<LevelControl>().actionLevel += points;
        }
        else {
            GameObject.Find("LevelController").GetComponent<LevelControl>().platLevel += points;
        }

        if (isFirst) {
            if (isCaos)
            {
                GameObject.Find("LevelController").GetComponent<LevelControl>().isAction = true;
            }
            else { 
            }
        }
    }

	
}
