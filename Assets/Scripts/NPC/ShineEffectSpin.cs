﻿using UnityEngine;
using System.Collections;

public class ShineEffectSpin : MonoBehaviour
{
    public float spinSpeed;//Default 40 e -40

    public void Update()
    {
        transform.Rotate(0, spinSpeed * Time.deltaTime, 0);
    }
}