﻿using UnityEngine;
using System.Collections;

public class GlitchAI : MonoBehaviour
{
    private Transform Abby;
    public bool inRange = false;
    private float rangeDist = 15;
    private Vector3 dist;
	private Vector3 restDist;
    private Vector3 spawnDist;

    public float wakeDist = 30;

    public GameObject shineFX;
    public GameObject warpEffect;

    private ParticleRenderer[] aRenderers;
    private Vector3 initialPos = Vector3.zero;
    private Vector3 glitchStartPos = Vector3.zero;
    public GameObject particlePrefab;
    private float energy = 100;
    public int difficultyExp = 2;
    public float currentEnergy;
    //public bool isHard = false;

    private float speed = 3;
    private float rotSpeed = 50;
    private bool isStopped = true;

    private bool isDead = false;
    private bool isSpawned = false;

    public void Start()
    {
        Abby = GameObject.FindWithTag("Player").transform;
        currentEnergy = energy * difficultyExp;
        glitchStartPos = transform.position;
        Sleep();
    }

    public void Sleep()
    {
        gameObject.animation.Stop();
        gameObject.GetComponent<AudioSource>().mute = true;
        isStopped = true;
        isSpawned = false;
    }

    public void Spawn()
    {
        gameObject.animation.Play();
        gameObject.GetComponent<AudioSource>().mute = false;
        rigidbody.AddRelativeForce(transform.up * 200);
        Instantiate(warpEffect, transform.position, transform.rotation);
        isSpawned = true;
    }

    public void Update()
    {
        spawnDist = Abby.position - transform.position;

        if (spawnDist.magnitude <= wakeDist)
        {
            if (!isSpawned)
            {
                Spawn();
            }
        }

        if (!isDead)
        {
            dist = Abby.position - transform.position;
            aRenderers = shineFX.GetComponentsInChildren<ParticleRenderer>();

            if (dist.magnitude <= rangeDist && Abby.GetComponent<Focus>().canFocus)
            {
                inRange = true;
                isStopped = false;
                ChaseState();

                foreach (ParticleRenderer render in aRenderers)
                {
                    render.enabled = true;
                }

                if (initialPos == Vector3.zero)
                {
                    initialPos = Abby.position;
                }

            }
            else if (dist.magnitude > rangeDist)
            {
                foreach (ParticleRenderer render in aRenderers)
                {
                    render.enabled = false;
                }

                restDist = transform.position - glitchStartPos;
                if (restDist.magnitude >= 5)
                {
                    isStopped = false;
                }
                ReturnPos();

                if (Abby.GetComponent<Focus>().target == transform || !Abby.GetComponent<Focus>().canFocus || Abby.GetComponent<Focus>().target == null)
                {
                    inRange = false;
                    Abby.SendMessage("FocusOff");

                    if (initialPos != Vector3.zero)
                    {
                        initialPos = Vector3.zero;
                    }
                }
            }
        }

        if (Abby.GetComponent<AbbyLife>().isDead)
        {
            if (GetComponent<AudioSource>())
            {
                GetComponent<AudioSource>().mute = true;
            }
        }
    }

    //Bater na Abby
    void OnCollisionEnter(Collision hit)
    {
        if (!isDead)
        {
            if (hit.collider.tag == "Player" && !Abby.GetComponent<Focus>().blinking)
            {
                inRange = false;
                Abby.SendMessage("FocusOff");
                ReturnPos();

                foreach (ParticleRenderer render in aRenderers)
                {
                    render.enabled = false;
                }

                //Dano
                Abby.SendMessage("TakeDamage");

                //Restaurar Vida Completa
                RestoreLife();

                hit.transform.position = initialPos;
                initialPos = Vector3.zero;
            }
        }
    }

    void Shatter()
    {
        inRange = false;
        Abby.SendMessage("FocusOff");
        foreach (ParticleRenderer render in aRenderers)
        {
            render.enabled = false;
        }
        initialPos = Vector3.zero;

        //ExplosionParticle
        Instantiate(particlePrefab,gameObject.transform.position,gameObject.transform.rotation);

        BecomeDummy();
    }

    void TakeDamage(float dam)
    {
        currentEnergy -= dam;
        if (currentEnergy <= 0)
        {
            Shatter();
        }
    }

    void RestoreLife()
    {
        currentEnergy = energy * difficultyExp;
        return;
    }

    void ChaseState()
    {
        var targetDir = Abby.transform.position - transform.position;

        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(targetDir), rotSpeed * Time.deltaTime);
        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
        rigidbody.velocity = targetDir.normalized * speed;
    }

    void ReturnPos()
    {
        if (!isStopped)
        {
            Instantiate(warpEffect, transform.position, transform.rotation);
            transform.position = glitchStartPos;
            isStopped = true;
        }
        return;
    }

    void BecomeDummy()
    {
        gameObject.animation.Stop();
        Destroy(gameObject.GetComponent<AudioSource>());
        isStopped = true;
        rigidbody.freezeRotation = false;

        gameObject.transform.LookAt(Abby.transform.position);

        Vector3 abbyDir = transform.position - Abby.transform.position;
        rigidbody.AddForceAtPosition(abbyDir.normalized * 300, transform.position);

        rigidbody.AddRelativeForce(transform.up * 400);

        int r1 = (int)UnityEngine.Random.Range(-5000, 5000);
        int r2 = (int)UnityEngine.Random.Range(-5000, 5000);
        rigidbody.AddRelativeTorque(transform.right * r1);
        rigidbody.AddRelativeTorque(transform.up * r2);

        isDead = true;
    }
}