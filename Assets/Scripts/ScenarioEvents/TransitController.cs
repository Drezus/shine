using UnityEngine;
using System.Collections;

public class TransitController : MonoBehaviour {
	
	public float delay;
	float delayRecover;
	public GameObject car;
	
	void Start () {
		delayRecover = delay;
	}
	
	void Update () {
		delay -= Time.deltaTime;
		if (delay <= 0) {
			Instantiate(car, transform.position, transform.rotation);
			delay = delayRecover;
		}
	}
}