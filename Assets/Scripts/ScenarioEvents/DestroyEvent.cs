using UnityEngine;
using System.Collections;

public class DestroyEvent : MonoBehaviour {

    public bool startAnim = false;

    void Update () {
        if (startAnim)
        {
            animation.Play();
            Destroy(gameObject, 2);
        }
    }
}
