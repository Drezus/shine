using UnityEngine;
using System.Collections;

public class CarXAnim : MonoBehaviour {
	
	public float xDistance;
	public float animTime;
	public float animDelay;
	public int animOrder;
	
	void Start () {
			iTween.MoveFrom (gameObject, iTween.Hash("x", transform.position.x+xDistance, "easetype", iTween.EaseType.easeOutQuart, "time", animTime, "delay", animDelay));
		}
	}
	
	

