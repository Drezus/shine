using UnityEngine;
using System.Collections;

public class TutPlatDestroyable : MonoBehaviour {

    public GameObject ball;

    void OnTriggerEnter(Collider hit) {
        if (hit.collider.tag == "Player") {
            Instantiate(ball, transform.FindChild("Spawn").position, transform.rotation);
        }
    }
}
