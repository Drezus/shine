using UnityEngine;
using System.Collections;

public class CarZAnim : MonoBehaviour {
	
	public float speed;
    public float zDistance;
    public bool shouldItStop = false;
	
	void Start () {
	
	}
	
	void Update () {
        if (shouldItStop)
        {
        }
        else
        {
            transform.Translate(speed * Vector3.right * Time.deltaTime);
            Destroy(gameObject, 15);
        }
	}
}
