using UnityEngine;
using System.Collections;

public class EventControl : MonoBehaviour {


    public enum levelType { None, ActionEnter, ActionExit, PlatformEnter, PlatformExit }
	public levelType chosenLevel;
	
    public enum eventType { None, CarStop, PeopleMovement }
	public eventType chosenEvent;
	
	GameObject upScenario;
	GameObject groundScenario;
	GameObject allCubes;
	public GameObject platformGroup;
    public GameObject peopleGroup;
    public GameObject redLight;
    public GameObject greenLight;
    public bool fadePeople;
	
	bool eventDone;

    public bool needKey;
    public enum keyTypes { None, Collectibles, Enemies }
	public keyTypes whatKey;
    public bool hasKey;
	public int howManyKeys;
	public int keyNumber;

	public Color fogCorruption;
    public Color fogChaos;
    Color fogFXColor;
	public Color fogReal;
    public Color fogEffectColor;
    public float bloomIntensity;
	public bool changeEffects = false;
	public float t = 0;
	bool reverseT = false;

    public float peopleWalkDistance;
    public float peopleWalkTime;

    private float timesUpStartOffset;

    public AudioSource abbyMusic;
    public AudioClip worldCrumble;
    public AudioClip apito;
	
	int collecNumber = 0;

	#region Effects
	float bloomIntensityReal;
	float bloomIntensityMemory;
	#endregion
	
	void Start () {
		bloomIntensityReal = 0.5f;
		bloomIntensityMemory = 2f;

        fogReal = GameObject.Find("EffectsController").GetComponent<EffectsControl>().colorReal;
        fogCorruption = GameObject.Find("EffectsController").GetComponent<EffectsControl>().colorCorruption;
        fogChaos = GameObject.Find("EffectsController").GetComponent<EffectsControl>().colorChaos;

		eventDone = false;
		upScenario = GameObject.Find("TimesSquareUp");
		groundScenario = GameObject.Find("Ground");
		allCubes = GameObject.Find("AllCubes");
		hasKey = false;
        keyNumber = 0;

        GameObject.Find("EffectsController").GetComponent<EffectsControl>().fogEffectColor = fogReal;
        timesUpStartOffset = GameObject.Find("TimesSquareUp").GetComponent<StartPos>().yOffset;

        GameObject.Find("TutParticle").GetComponent<ParticleEmitter>().emit = false;
	}
	
	
	void Update () {
		Mathf.Clamp(keyNumber, 0, howManyKeys+1);	
		
		bloomIntensity = Mathf.Lerp (bloomIntensityReal, bloomIntensityMemory, t);
        GameObject.Find("EffectsController").GetComponent<EffectsControl>().bloomIntensity = bloomIntensity;
        fogEffectColor = Color.Lerp(fogReal, fogFXColor, t);

        t = GameObject.Find("EffectsController").GetComponent<EffectsControl>().mainT;
		if (changeEffects == true) {
			if (!reverseT)	if (t<1) t += Time.deltaTime;
			else if (reverseT)	if (t>0) t -= Time.deltaTime;
            GameObject.Find("EffectsController").GetComponent<EffectsControl>().fogEffectColor = fogEffectColor;
            GameObject.Find("EffectsController").GetComponent<EffectsControl>().mainT = t;
		}
        
		
		if (needKey) {
			switch (whatKey){
			case keyTypes.None:
				break;
				
			case keyTypes.Collectibles:
				Collectibles();
				break;
				
			case keyTypes.Enemies:
				Enemies();
				break;
			}
		} else {
			
		}
	}
	
	void OnTriggerEnter (Collider hit) {
		if (hit.collider.tag == "Player") {
			if (!eventDone) {
				switch (chosenLevel) {
					
				case levelType.None:
					break;
					
				case levelType.ActionEnter:
					KeyChecker("ActionEnter");
					break;
					
				case levelType.ActionExit:
					KeyChecker("ActionExit");
					break;
					
				case levelType.PlatformEnter:
					KeyChecker("PlatEnter");
					break;
					
				case levelType.PlatformExit:
					KeyChecker("PlatExit");
					break;
				}
				
				switch (chosenEvent) {
					
				case eventType.None:
					break;
					
				case eventType.CarStop:
					break;

                case eventType.PeopleMovement:
                    KeyChecker("PeopleMove");
                    break;
				}
			}
		}
	}
	
	void Collectibles () {
		if (keyNumber >= howManyKeys) {hasKey = true;}
	}
	
	void Enemies () {
		if (keyNumber >= howManyKeys) {hasKey = true;}
	}
	
	void KeyChecker (string levelName) {
		if (!needKey) {
			if (levelName == "PlatEnter") PlatFXEnter();
			if (levelName == "PlatExit") PlatFXExit();
			if (levelName == "ActionEnter") ActionFXEnter();
			if (levelName == "ActionExit") ActionFXExit();
            if (levelName == "PeopleMove") PeopleMove();
			eventDone = true;
		} else {
			if (hasKey) {
				if (levelName == "PlatEnter") PlatFXEnter();
				if (levelName == "PlatExit") PlatFXExit();
				if (levelName == "ActionEnter") ActionFXEnter();
				if (levelName == "ActionExit") ActionFXExit();
                if (levelName == "PeopleMove") PeopleMove();
				eventDone = true;
			} else {}
			
		}
	}
	
	void PlatFXEnter () {
        reverseT = false;
        fogFXColor = fogCorruption;
		changeEffects = true;
        iTween.MoveTo(upScenario, iTween.Hash("y", upScenario.transform.position.y - 1500, "time", 4, "easetype", iTween.EaseType.easeOutCubic));
        iTween.MoveTo(allCubes, iTween.Hash("y", allCubes.transform.position.y + 1500, "time", 4, "easetype", iTween.EaseType.easeOutCubic));
        iTween.MoveTo(groundScenario, iTween.Hash("y", groundScenario.transform.position.y - 300, "time", 3.5f, "easetype", iTween.EaseType.easeInCubic));
        GameObject.Find("Snow").GetComponent<ParticleEmitter>().emit = false;
        GameObject.Find("TutParticle").GetComponent<ParticleEmitter>().emit = true;
        abbyMusic.PlayOneShot(worldCrumble);
        Invoke("StopChanges", 4);
	}
	
	void PlatFXExit () {
        reverseT = true;
        fogFXColor = fogReal;
		changeEffects = true;
        iTween.MoveTo(upScenario, iTween.Hash("y", upScenario.transform.position.y + 1500, "time", 2, "easetype", iTween.EaseType.easeInCubic));
        iTween.MoveTo(allCubes, iTween.Hash("y", allCubes.transform.position.y - 1500, "time", 2, "easetype", iTween.EaseType.easeInCubic));
        iTween.MoveTo(groundScenario, iTween.Hash("y", groundScenario.transform.position.y + 300, "time", 2, "easetype", iTween.EaseType.easeOutCubic));
        GameObject.Find("Snow").GetComponent<ParticleEmitter>().emit = true;
        GameObject.Find("TutParticle").GetComponent<ParticleEmitter>().emit = false;
        abbyMusic.PlayOneShot(worldCrumble);
        Invoke("StopChanges", 4);
	}
	
	void ActionFXEnter () {
        fogFXColor = fogChaos;
        reverseT = false;
        changeEffects = true;
        iTween.MoveTo(upScenario, iTween.Hash("y", upScenario.transform.position.y - timesUpStartOffset, "time", 4, "easetype", iTween.EaseType.easeOutCubic));
        iTween.MoveTo(allCubes, iTween.Hash("y", allCubes.transform.position.y + timesUpStartOffset, "time", 4, "easetype", iTween.EaseType.easeOutCubic));
        GameObject.Find("Snow").GetComponent<ParticleEmitter>().emit = false;
        GameObject.Find("TutParticle").GetComponent<ParticleEmitter>().emit = true;
        abbyMusic.PlayOneShot(worldCrumble);
		Destroy(gameObject,4);
	}
	
	void ActionFXExit () {
        fogFXColor = fogReal;
        reverseT = true;
        changeEffects = true;
        iTween.MoveTo(upScenario, iTween.Hash("y", upScenario.transform.position.y + timesUpStartOffset, "time", 2, "easetype", iTween.EaseType.easeInCubic));
        iTween.MoveTo(allCubes, iTween.Hash("y", allCubes.transform.position.y - timesUpStartOffset, "time", 2, "easetype", iTween.EaseType.easeInCubic));
        GameObject.Find("Snow").GetComponent<ParticleEmitter>().emit = true;
        GameObject.Find("TutParticle").GetComponent<ParticleEmitter>().emit = false;
        abbyMusic.PlayOneShot(worldCrumble);
		Destroy(gameObject,4);
	}

    void PeopleMove() {
        iTween.MoveTo(peopleGroup, iTween.Hash("z", peopleGroup.transform.position.z + peopleWalkDistance, "easetype", iTween.EaseType.linear, "time", peopleWalkTime));
        fadePeople = true;
        greenLight.SetActive(true);
        redLight.SetActive(false);
        abbyMusic.PlayOneShot(apito);
    }

    void StopChanges() {
        changeEffects = false;
    }
}
