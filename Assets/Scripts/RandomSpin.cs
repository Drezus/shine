﻿using UnityEngine;
using System.Collections;

public class RandomSpin : MonoBehaviour
{
    private float r1;
    private float r2;
    private float r3;

    public void Awake()
    {
        r1 = r2 = r3 = (int)UnityEngine.Random.Range(-20, 20);
    }

    public void Update()
    {
        transform.Rotate(r1 * Time.deltaTime, r2 * Time.deltaTime, r3 * Time.deltaTime);
    }
}