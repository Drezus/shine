﻿using UnityEngine;

public class DiagBoxControl : MonoBehaviour
{
    public GameObject mugshot;

    public Texture stan;
    public Texture patrick;
    public Texture nateVoid;
    public Texture abbySad;
    public Texture abbyAngry;

    public GUIText diagText;

    public AudioSource abbyMusic;
    public AudioClip foge;

    public void Start()
    {
        diagText.text = "";

        if (Application.loadedLevelName == "TimesIntro")
        {
            IntroDialog();
        }

        if (Application.loadedLevelName == "TimesSquare")
        {
            AloneDialog();
        }
        
    }

    public void Update()
    {

    }

    public void AloneDialog()
    {
        Invoke("AloneDiag1", 0.5f);
        Invoke("AloneDiag2", 7);
        Invoke("AloneDiag3", 12);
    }

    public void NateFirstDialog()
    {
        NathanFirstDiag1();
        Invoke("NathanFirstDiag2", 6);
    }

    public void NateSecondDialog()
    {
        NathanSecondDiag1();
        Invoke("NathanSecondDiag2", 6);
    }

    public void NateThirdDialog()
    {
        NathanThirdDiag1();
        Invoke("NathanThirdDiag2", 6);
    }

    public void NateFinalDialog()
    {
        NathanFinalDiag1();
        Invoke("NathanFinalDiag2", 6);
    }

    public void PlatDialog()
    {
        PlatDiag1();
        Invoke("PlatDiag2", 9);
        Invoke("PlatDiag3", 18);
        Invoke("PlatDiag4", 25);
        Invoke("PlatDiag5", 32);
        Invoke("PlatDiag6", 39);
    }

    public void IntroDialog()
    {
        Invoke("IntroDiag1", 0.5f);
        Invoke("IntroDiag2", 5);
        Invoke("IntroDiag3", 10);
        Invoke("IntroDiag4", 15);
        Invoke("IntroDiag5", 20);
    }

    public void BattleDialog()
    {
        BattleDiag1();
        Invoke("BattleDiag2",6);
    }


#region Dialogue Engine

    public void PopDialog(string dialog, Texture mug, float duration)
    {
        ShowBox();
        ChangeText(dialog);
        ChangeMug(mug);

        Invoke("HideBox", duration);
    }



    public void ShowBox()
    {
        iTween.MoveTo(gameObject, iTween.Hash("y", 0.16f, "time", 1, "easetype", iTween.EaseType.easeOutQuart));
    }

    public void HideBox()
    {
        iTween.MoveTo(gameObject, iTween.Hash("y", -0.50f, "time", 1, "easetype", iTween.EaseType.easeInQuart));
    }

    public void ChangeText(string diag)
    {
        diagText.text = diag;
    }

    public void ChangeMug(Texture mugTexture)
    {
        mugshot.guiTexture.texture = mugTexture;
    }

#endregion


#region All Dialogue Functions

    #region Platform Dialogues

    public void PlatDiag1()
    {
        PopDialog("Patrick, não me diga que você quase derrubou um dos \ncomputadores no chão! Você tem noção de quanto o \nDr. Mierzquiak ficaria bravo se quebrássemos qualquer \num desses aparelhos aqui?", stan, 8);
    }

    public void PlatDiag2()
    {
        PopDialog("Desculpe, Stan, eu tropecei no fio que acabou arrastando \num pouco ele aí em cima da mesa. Tirando o barulho,\nnão deu em nada. Pelo menos é o que eu espero.", patrick, 8);
    }

    public void PlatDiag3()
    {
        PopDialog("Cara! Toma cuidado com esse equipamento. Você não vai querer \nqueimar o cérebro dessa garota linda aqui, não é?", stan, 6);
    }

    public void PlatDiag4()
    {
        PopDialog("Espero que não dê em nada. O doutor não vai gostar nada se \nnós causarmos algum tipo de trauma nessa garota.", patrick, 6);
    }

    public void PlatDiag5()
    {
        PopDialog("Bom, se isso acontecer, ela pode apagar a memória de novo e \nnão se lembrará de nada!", stan, 6);
    }

    public void PlatDiag6()
    {
        PopDialog("Hahahahahahaha, é mesmo! \n...Mas espero que não cheguemos a esse ponto.", patrick, 7);
    }

    #endregion

    #region Alone Dialogues
    public void AloneDiag1()
    {
        PopDialog("...?\nPorque estou aqui... \n...de novo?", abbySad, 5.5f);
    }
    public void AloneDiag2()
    {
        PopDialog("“...Nã _  Abb _ , n _ o v _ mos a lug _ r nenh _ m. \nEu t _ nho q _ e r _ solv _ r i s_ o aq _ i...”", nateVoid, 4f);
    }
    public void AloneDiag3()
    {
        PopDialog("...Nathan?! \n\nSerá que...?", abbyAngry, 4f);
    }
    #endregion

    #region Nathan First Dialogues
    public void NathanFirstDiag1()
    {
        PopDialog("“...Ab _ ga _ l... Vo _ ê q _ er namo _ ar c _ mig _ ?”", nateVoid, 5f);
    }
    public void NathanFirstDiag2()
    {
        PopDialog("Nathan... Saia da minha vida de uma vez!", abbySad, 5f);
    }
    #endregion

    #region Nathan Second Dialogues
    public void NathanSecondDiag1()
    {
        PopDialog("“...S _ be Ab _ y, v _ cê me f _ z sent _ r tã _  b _ m...”", nateVoid, 5f);
    }
    public void NathanSecondDiag2()
    {
        PopDialog(". . .", abbySad, 2.5f);
    }
    #endregion

    #region Nathan Third Dialogues
    public void NathanThirdDiag1()
    {
        PopDialog("“...Ach _  q _ e se _  c _ mo vo _ ê se s _ nte, Abb _ .”", nateVoid, 5f);
    }
    public void NathanThirdDiag2()
    {
        PopDialog(". . .", abbySad, 2.5f);
    }
    #endregion

    #region Nathan Final Dialogues
    public void NathanFinalDiag1()
    {
        PopDialog("“Eu t _ pr _ meto, min _ a A _ by.”", nateVoid, 5f);
    }
    public void NathanFinalDiag2()
    {
        PopDialog("Nathan... \nPor favor, pare...", abbySad, 5f);
    }
    #endregion

    #region Intro Dialogues

    public void IntroDiag1()
    {
        PopDialog("Nate, você não gosta mais de mim?", abbyAngry, 3.5f);
    }

    public void IntroDiag2()
    {
        PopDialog("Abby! Não fale uma coisa dessas! Por que pergunta isso?! \nVocê não confia mais em mim, é?", nateVoid, 4);
    }

    public void IntroDiag3()
    {
        PopDialog("Eu confio mas... Você me evita, fica muito tempo sem me ver, \neu não sei nem o que pensar.", abbySad, 4);
    }

    public void IntroDiag4()
    {
        PopDialog("Você só me pressiona, Abby, nunca me dá espaço! O que quer \nque eu faça? Já fiz de tudo!", nateVoid, 4);
    }

    public void IntroDiag5()
    {
        PopDialog("Argh!! \n\nEu...   \nTE ODEIO!", abbyAngry, 3);
        abbyMusic.PlayOneShot(foge, 0.5f);
    }

    #endregion

    #region Battle Dialogues

    public void BattleDiag1()
    {
        PopDialog("Stan, e se eu quebrei o aparelho?", patrick, 5);
    }

    public void BattleDiag2()
    {
        PopDialog("Acho que não chegou a esse extremo, mas devo lhe dizer \nisso, Patrick: as consequências desse erro só ela saberá. \nVamos continuar apagando as memórias dela para podermos \nterminar com isso o mais rápido possível.", stan, 11);
    }

    #endregion

#endregion


}