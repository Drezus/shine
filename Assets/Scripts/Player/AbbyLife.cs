﻿using UnityEngine;

public class AbbyLife : MonoBehaviour
{
    public float life = 5;
    public AudioSource music;
    public AudioClip gameOverMusic;
    public AudioClip damageSFX;
    public AudioClip healSFX;
    public AudioClip hurt1SFX;
    public AudioClip hurt2SFX;
    public AudioClip jump1SFX;
    public AudioClip jump2SFX;
    public AudioClip deathCry;

    public GameObject whiteFlash;
    public GameObject damageFlash;

    public GameObject incubo;
    public GameObject presents;

    public GameObject deathFade;
    public GameObject deathParticles;
    public GameObject deathPhrase1;
    public GameObject deathPhrase2;

    public GameObject life1;
    public GameObject life2;
    public GameObject life3;
    public GameObject life4;
    public GameObject life5;
    public GameObject[] lifeArray;

    public Animator abbyAnim;

    public GameObject abbyRoot;

    public GameObject cheatPos;

    public bool isDead = false;

    public void Start()
    {
        lifeArray = GameObject.FindGameObjectsWithTag("lifeIcon");
        HideLife();
        if (Application.loadedLevelName == "TimesIntro")
        {
            Invoke("StageIntroFade", 30);
            Invoke("IntroSound", 24);
        }
        else
        {
            StageIntroFade();
        }

        abbyAnim.SetLayerWeight(1, 5);
    }

    public void Update()
    {
        if (!whiteFlash.animation.isPlaying)
        {
            whiteFlash.SetActive(false);
        }

        if (!damageFlash.animation.isPlaying)
        {
            damageFlash.SetActive(false);
        }

    #region Coisas das vidas-bolas
        if (life == 0)
        {
            life1.renderer.enabled = life1.GetComponent<Light>().enabled = life1.GetComponent<ParticleEmitter>().emit = false;
            life2.renderer.enabled = life2.GetComponent<Light>().enabled = life2.GetComponent<ParticleEmitter>().emit = false;
            life3.renderer.enabled = life3.GetComponent<Light>().enabled = life3.GetComponent<ParticleEmitter>().emit = false;
            life4.renderer.enabled = life4.GetComponent<Light>().enabled = life4.GetComponent<ParticleEmitter>().emit = false;
            life5.renderer.enabled = life5.GetComponent<Light>().enabled = life5.GetComponent<ParticleEmitter>().emit = false;
        }
        if (life == 1)
        {
            life1.renderer.enabled = life1.GetComponent<Light>().enabled = life1.GetComponent<ParticleEmitter>().emit = true;
            life2.renderer.enabled = life2.GetComponent<Light>().enabled = life2.GetComponent<ParticleEmitter>().emit = false;
            life3.renderer.enabled = life3.GetComponent<Light>().enabled = life3.GetComponent<ParticleEmitter>().emit = false;
            life4.renderer.enabled = life4.GetComponent<Light>().enabled = life4.GetComponent<ParticleEmitter>().emit = false;
            life5.renderer.enabled = life5.GetComponent<Light>().enabled = life5.GetComponent<ParticleEmitter>().emit = false;
        }
        if (life == 2)
        {
            life1.renderer.enabled = life1.GetComponent<Light>().enabled = life1.GetComponent<ParticleEmitter>().emit = true;
            life2.renderer.enabled = life2.GetComponent<Light>().enabled = life2.GetComponent<ParticleEmitter>().emit = true;
            life3.renderer.enabled = life3.GetComponent<Light>().enabled = life3.GetComponent<ParticleEmitter>().emit = false;
            life4.renderer.enabled = life4.GetComponent<Light>().enabled = life4.GetComponent<ParticleEmitter>().emit = false;
            life5.renderer.enabled = life5.GetComponent<Light>().enabled = life5.GetComponent<ParticleEmitter>().emit = false;
        }
        if (life == 3)
        {
            life1.renderer.enabled = life1.GetComponent<Light>().enabled = life1.GetComponent<ParticleEmitter>().emit = true;
            life2.renderer.enabled = life2.GetComponent<Light>().enabled = life2.GetComponent<ParticleEmitter>().emit = true;
            life3.renderer.enabled = life3.GetComponent<Light>().enabled = life3.GetComponent<ParticleEmitter>().emit = true;
            life4.renderer.enabled = life4.GetComponent<Light>().enabled = life4.GetComponent<ParticleEmitter>().emit = false;
            life5.renderer.enabled = life5.GetComponent<Light>().enabled = life5.GetComponent<ParticleEmitter>().emit = false;
        }
        if (life == 4)
        {
            life1.renderer.enabled = life1.GetComponent<Light>().enabled = life1.GetComponent<ParticleEmitter>().emit = true;
            life2.renderer.enabled = life2.GetComponent<Light>().enabled = life2.GetComponent<ParticleEmitter>().emit = true;
            life3.renderer.enabled = life3.GetComponent<Light>().enabled = life3.GetComponent<ParticleEmitter>().emit = true;
            life4.renderer.enabled = life4.GetComponent<Light>().enabled = life4.GetComponent<ParticleEmitter>().emit = true;
            life5.renderer.enabled = life5.GetComponent<Light>().enabled = life5.GetComponent<ParticleEmitter>().emit = false;
        }
        if (life == 5)
        {
            life1.renderer.enabled = life1.GetComponent<Light>().enabled = life1.GetComponent<ParticleEmitter>().emit = true;
            life2.renderer.enabled = life2.GetComponent<Light>().enabled = life2.GetComponent<ParticleEmitter>().emit = true;
            life3.renderer.enabled = life3.GetComponent<Light>().enabled = life3.GetComponent<ParticleEmitter>().emit = true;
            life4.renderer.enabled = life4.GetComponent<Light>().enabled = life4.GetComponent<ParticleEmitter>().emit = true;
            life5.renderer.enabled = life5.GetComponent<Light>().enabled = life5.GetComponent<ParticleEmitter>().emit = true;
        }
#endregion 

        abbyAnim.rootPosition = abbyRoot.transform.position;
        abbyAnim.rootRotation = abbyRoot.transform.rotation;

        if (Input.GetKeyDown(KeyCode.Escape) && !GetComponent<CinematicControl>().inEvent && Application.loadedLevelName == "TimesSquare")
        {
            Application.LoadLevel("Main Menu");
        }

        if (Input.GetKeyDown(KeyCode.Escape) && Application.loadedLevelName == "TimesIntro")
        {
            Application.LoadLevel("TimesSquare");
        }

        if (Input.GetKeyDown(KeyCode.F1))
        {
            Application.LoadLevel(Application.loadedLevel);
        }

        if (Input.GetKeyDown(KeyCode.F5))
        {
            transform.position = cheatPos.transform.position;
        }

    }

    void IntroSound()
    {
        Invoke("IncuboPresents", 1);
    }

    void IncuboPresents()
    {
        incubo.SetActive(true);
        presents.SetActive(true);
    }

    void TakeDamage()
    {
        GetComponent<Focus>().blinking = true;

        abbyAnim.SetBool("TakeDamage", true);
        Invoke("TakeDamageFalse", 0.1f);

        if(life>1)music.PlayOneShot(damageSFX);
        DamageFlash();
        ShowLife();

        int r = (int)UnityEngine.Random.Range(1, 3);

        if (r == 1)
        {
            music.PlayOneShot(hurt1SFX);
        }
        if (r == 2)
        {
            music.PlayOneShot(hurt2SFX);
        }

        life -= 1;
        if (life <= 0)
        {
            Invoke("DeathSequence", 0.1f);

            abbyAnim.SetBool("Dead", true);
            Invoke("DeadFalse", 0.1f);

            abbyAnim.SetBool("EndDamage", true);
            Invoke("EndDamageFalse", 0.1f);
        }
    }

    void DeadFalse()
    {
        abbyAnim.SetBool("Dead", false);
    }

    void TakeDamageFalse()
    {
        abbyAnim.SetBool("TakeDamage", false);
    }

    void EndDamageFalse()
    {
        abbyAnim.SetBool("EndDamage", false);
    }

    void RestoreLife()
    {
        music.PlayOneShot(healSFX);

        life += 1;
        if (life >= 5)
        {
            life = 5;
        }
    }

    public void OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.tag == "checkpoint")
        {
            SendMessage("SetLSKP");
        }

        if (hit.gameObject.tag == "pit")
        {
            TakeDamage();
            SendMessage("ReturnToLSKP");
        }

        if (hit.gameObject.tag == "health" && life < 5)
        {
            RestoreLife();
            ShowLife();
            Destroy(hit.gameObject);
        }

        if (hit.gameObject.tag == "resetPortal")
        {
            Application.LoadLevel("Main Menu");
        }
    }


    public void Jumped()
    {
        int r = (int)UnityEngine.Random.Range(1, 3);

        if (r == 1)
        {
            music.PlayOneShot(jump1SFX);
        }
        if (r == 2)
        {
            music.PlayOneShot(jump2SFX);
        }

    	abbyAnim.SetBool("Jump", true);
        Invoke("JumpFalse", 0.1f);
    }

    private void JumpFalse()
    {
        abbyAnim.SetBool("Jump", false);
    }

    public void DamageFlash()
    {
        damageFlash.SetActive(true);
        damageFlash.animation.Play();
    }

    public void DeathFade()
    {
        deathFade.SetActive(true);
        deathFade.animation.Play();
    }

    public void StageIntroFade()
    {
       deathFade.SetActive(true);
       deathFade.animation.enabled = true;
       deathFade.animation.Play("GUIStageFadeIn");
       Invoke("DisableDeathGUI", 9);
    }

    public void DeathParticles()
    {
        deathParticles.SetActive(true);
        deathParticles.GetComponent<ParticleEmitter>().emit = true;
    }

    public void DeathPhrase1()
    {
        deathPhrase1.SetActive(true);
        deathPhrase1.animation.Play();
    }

    public void DeathPhrase2()
    {
        deathPhrase2.SetActive(true);
        deathPhrase2.animation.Play();
    }

    public void DeathSequence()
    {
        isDead = true;
        GetComponent<CharacterController>().enabled = false;
        GetComponent<PlatformInputController>().enabled = false;
        music.Stop();
        music.PlayOneShot(damageSFX);
        music.PlayOneShot(deathCry);
        music.PlayOneShot(gameOverMusic);
        Invoke("DeathParticles", 3);
        Invoke("DeathFade", 3);
        Invoke("DeathPhrase1", 5);
        Invoke("DeathPhrase2", 7);
        GetComponent<CinematicControl>().DeathSequence();
    }

    public void WhiteFlashIn()
    {
        whiteFlash.SetActive(true);
        whiteFlash.animation.Play();
    }

    public void WhiteFlashOut()
    {
        whiteFlash.SetActive(true);
        whiteFlash.animation.Play("GUIFlashOut");
    }

    public void DisableDeathGUI()
    {
        deathFade.SetActive(false);
    }


    public void ShowLife()
    {
        foreach (GameObject icon in lifeArray)
        {
            icon.animation.Play("lifeFadeIn");
            Invoke("HideLife", 5);
        }
    }

    public void HideLife()
    {
        foreach (GameObject icon in lifeArray)
        {
            icon.animation.Play("lifeFadeOut");
        }
    }
}