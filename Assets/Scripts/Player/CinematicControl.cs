﻿using UnityEngine;

public class CinematicControl : MonoBehaviour
{
    public AudioSource music;
    public CharacterController abbyController;
    public PlatformInputController abbyPlat;

    public AudioClip introSFX;

    public GameObject MainCamera;
    public GameObject MainCameraPos;
    public GameObject crosshair;
    public GameObject diagBox;

    public GameObject shineLogo;

    public GameObject introPos1;
    public GameObject introPos2;
    public GameObject introPos3;
    public GameObject introPos4;
    public GameObject introPos5;
    public GameObject introPos6;
    public GameObject introPos7;
    public GameObject introPos8;

    public GameObject credits1;
    public GameObject credits2;
    public GameObject credits3;

    public GameObject battlePos1;
    public GameObject battlePos2;

    public GameObject platPos1;
    public GameObject platPos2;

    public GameObject deathPos1;
    public GameObject deathPos2;

    public GameObject whiteFlash;
    public GameObject damageFlash;

    public bool inEvent = false;

    public void Start()
    {
        damageFlash.SetActive(false);

        if (Application.loadedLevelName == "TimesIntro")
        {
            Invoke("WhiteFlashOut", 24);
            Invoke("IntroSequence", 30);
        }
            
    }

    public void Update()
    {
        if (!whiteFlash.animation.isPlaying)
        {
            whiteFlash.SetActive(false);
        }

        if (!damageFlash.animation.isPlaying)
        {
            damageFlash.SetActive(false);
        }

        if (!inEvent)
        {
            MainCameraPos.transform.position = MainCamera.transform.position;
            MainCameraPos.transform.rotation = MainCamera.transform.rotation;          
        }

        abbyController.enabled = abbyPlat.enabled = !inEvent;
    }

    public void OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.tag == "cinematic")
        {
            if (hit.gameObject.GetComponent<CinematicID>().Sequence == "Glitch")
            {
                DisableCamera();
                Invoke("BattleSequence", 0.2f);
                diagBox.SendMessage("BattleDialog", SendMessageOptions.DontRequireReceiver);
            }
            if (hit.gameObject.GetComponent<CinematicID>().Sequence == "Plat")
            {
                DisableCamera();
                Invoke("PlatSequence", 0.2f);
                diagBox.SendMessage("PlatDialog", SendMessageOptions.DontRequireReceiver);

            }
            if (hit.gameObject.GetComponent<CinematicID>().Sequence == "NateFirst")
            {
                diagBox.SendMessage("NateFirstDialog", SendMessageOptions.DontRequireReceiver);
            }
            if (hit.gameObject.GetComponent<CinematicID>().Sequence == "NateSecond")
            {
                diagBox.SendMessage("NateSecondDialog", SendMessageOptions.DontRequireReceiver);
            }
            if (hit.gameObject.GetComponent<CinematicID>().Sequence == "NateThird")
            {
                diagBox.SendMessage("NateThirdDialog", SendMessageOptions.DontRequireReceiver);
            }
            if (hit.gameObject.GetComponent<CinematicID>().Sequence == "NateFinal")
            {
                diagBox.SendMessage("NateFinalDialog", SendMessageOptions.DontRequireReceiver);
            }
            hit.gameObject.tag = "cinematicDONE";
        }
    }

    public void IntroSequence()
    {
        inEvent = true;
        MainCamera.GetComponent<MouseOrbit>().enabled = false;
        IntroPan1();
        Invoke("IntroPan2", 7);
        Invoke("IntroPan3", 14);
        Invoke("IntroPan4", 21);
        Invoke("IntroPan5", 27);
    }

    public void BattleSequence()
    {
        BattlePan1();
        Invoke("BattlePan2", 2);
        Invoke("BattlePan3", 20);
    }

    public void PlatSequence()
    {
        PlatPan1();
        Invoke("PlatPan2", 2);
        Invoke("PlatPan3", 16);
    }


    public void DamageFlash()
    {
        damageFlash.SetActive(true);
        damageFlash.animation.Play();
    }

    public void DeathSequence()
    {
        DisableCamera();
        DeathPan1();
        Invoke("DeathPan2", 0.7f);
        Invoke("DeathPan3", 10);
    }

    #region Event Camera Engine

    public void TweenCameraTo(GameObject target, float duration)
    {
        iTween.MoveTo(MainCamera, iTween.Hash("position", target.transform.position, "time", duration, "easetype", iTween.EaseType.easeInOutQuad));
        iTween.RotateTo(MainCamera, iTween.Hash("rotation", target.transform.eulerAngles, "time", duration, "easetype", iTween.EaseType.easeInOutQuad));
    }

    public void LinearCameraTo(GameObject target, float duration)
    {
        iTween.MoveTo(MainCamera, iTween.Hash("position", target.transform.position, "time", duration, "easetype", iTween.EaseType.linear));
        iTween.RotateTo(MainCamera, iTween.Hash("rotation", target.transform.eulerAngles, "time", duration, "easetype", iTween.EaseType.linear));
    }

    public void JumpCameraTo(GameObject target)
    {
        MainCamera.transform.position = target.transform.position;
        MainCamera.transform.rotation = target.transform.rotation;
    }

    public void ReturnToPlayer(float duration)
    {
        iTween.MoveTo(MainCamera, iTween.Hash("position", MainCameraPos.transform.position, "time", duration, "easetype", iTween.EaseType.easeInOutQuad));
        iTween.RotateTo(MainCamera, iTween.Hash("rotation", MainCameraPos.transform.eulerAngles, "time", duration, "easetype", iTween.EaseType.easeInOutQuad));
        Invoke("EnableCamera", duration);
    }

    public void DisableCamera()
    {
        inEvent = true;
        WhiteFlashOut();
        Invoke("WhiteFlashIn", 0.2f);
        MainCamera.GetComponent<MouseOrbit>().enabled = false;
        crosshair.gameObject.SetActive(false);
    }

    public void EnableCamera()
    {
        inEvent = false;
        WhiteFlashOut();
        Invoke("WhiteFlashIn", 0.2f);
        MainCamera.GetComponent<MouseOrbit>().enabled = true;
        crosshair.gameObject.SetActive(true);
    }

    public void WhiteFlashIn()
    {
        whiteFlash.SetActive(true);
        whiteFlash.animation.Play();
    }

    public void WhiteFlashOut()
    {
        whiteFlash.SetActive(true);
        whiteFlash.animation.Play("GUIFlashOut");
        if (Application.loadedLevelName == "TimesIntro")
        {
            music.PlayOneShot(introSFX);
        }
    }

    #endregion



    #region All Event Pans

    #region Intro Pans

    public void IntroPan1()
    {
        JumpCameraTo(introPos1);
        LinearCameraTo(introPos2, 6.9f);
        
    }

     public void IntroPan2()
     {
        JumpCameraTo(introPos3);
        LinearCameraTo(introPos4, 6.9f);
        credits1.SetActive(true);
        shineLogo.SetActive(false);
    }

     public void IntroPan3()
     {
        JumpCameraTo(introPos5);
        LinearCameraTo(introPos6, 6.9f);
        credits1.SetActive(false);
        credits2.SetActive(true);
        
    }
     public void IntroPan4()
     {
        JumpCameraTo(introPos7);
        TweenCameraTo(introPos8, 4);
        credits2.SetActive(false);
        credits3.SetActive(true);
        
    }
     public void IntroPan5()
     {
         GetComponent<AbbyLife>().DeathFade();
         Invoke("IntroPan6", 4f);
     }

     public void IntroPan6()
     {
         Application.LoadLevel("TimesSquare");
     }

    #endregion

    #region Death Pans

    public void DeathPan1()
    {
        TweenCameraTo(deathPos1, 0.5f);
    }

    public void DeathPan2()
    {
        TweenCameraTo(deathPos2, 8);
    }

    public void DeathPan3()
    {
        Application.LoadLevel(Application.loadedLevel);
    }

    #endregion

    #region Battle Pans

    public void BattlePan1()
    {
        TweenCameraTo(battlePos1, 1);
    }

    public void BattlePan2()
    {
        TweenCameraTo(battlePos2, 18);
    }

    public void BattlePan3()
    {
        ReturnToPlayer(1);
    }

    #endregion

    #region Platform Pans

    public void PlatPan1()
    {
        SendMessage("TakeDamage");
        
        TweenCameraTo(platPos1, 1);
    }

    public void PlatPan2()
    {
        TweenCameraTo(platPos2, 15);
    }

    public void PlatPan3()
    {
        ReturnToPlayer(2);
        SendMessage("RestoreLife");
    }

    #endregion

    #endregion










}

