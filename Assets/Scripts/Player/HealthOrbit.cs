﻿using UnityEngine;
using System.Collections;

public class HealthOrbit : MonoBehaviour
{

    public Transform target;
    private float orbitDistance = 12;
    private float orbitDegreesPerSec = 80;
    public Vector3 relativeDistance = Vector3.zero;

    // Use this for initialization
    void Start()
    {

        if (target != null)
        {
            relativeDistance = transform.position - target.position;
        }
    }

    void Orbit()
    {
        if (target != null)
        {
            // Keep us at the last known relative position
            transform.position = target.position + relativeDistance;
            transform.RotateAround(target.position, Vector3.up, orbitDegreesPerSec * Time.deltaTime);
            // Reset relative position after rotate
            relativeDistance = transform.position - target.position;
        }
    }

    void LateUpdate()
    {

        Orbit();

    }
}