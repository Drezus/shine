﻿using UnityEngine;
using System.Collections;

public class Focus : MonoBehaviour
{
    public Transform target = null;
	
    public Light spot;
    public Light directional;
	
    public Transform Abby;
    public bool inFocus = false;
    public bool canFocus = true;

    public GameObject hardCircle;

    public bool blinking = false;
    private float blink;
    public float blinkTime = 4;
    private float blinkInterval;
	
	public Camera mainCam;
	public Camera battleCam;

    public AudioSource music;

    public static GameObject[] aCircles;
    public static GameObject[] enemyMusic;

    public float damBase = 25;

    public Texture overEnemyTexture;
    public Texture defaultPointerTexture;
    public GUITexture crosshair;

    public Animator abbyAnim;

    //public MovieTexture video1;

    public void Start()
    {
        spot.enabled = false;
        directional.enabled = true;

        music.GetComponent<AudioLowPassFilter>().enabled = false;
        music.GetComponent<AudioChorusFilter>().enabled = false;
        mainCam.GetComponent<MotionBlur>().enabled = false;
        mainCam.GetComponent<NoiseEffect>().enabled = false;
        mainCam.GetComponent<Vignetting>().enabled = false;

        enemyMusic = GameObject.FindGameObjectsWithTag("enemy");
        foreach (GameObject enemy in enemyMusic)
        {
            enemy.GetComponent<AudioLowPassFilter>().enabled = false;
        }
		
		mainCam.enabled = true;
		battleCam.enabled = false;

        Screen.lockCursor = true;

        crosshair.texture = defaultPointerTexture;   
     }

    public void Update()
    {
        //if (!video1.isPlaying)
        //{
        //    video1.Play();
        //    video1.loop = true;
        //}

        #region Raycast Stuff
        if (!target)
        {
            RaycastHit hit;
            Vector3 screenMiddle = new Vector3(Screen.width / 2, Screen.height / 2, 0);
            Ray ray = Camera.main.ScreenPointToRay(screenMiddle);
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.tag == "enemy")
                {
                    if (hit.collider.GetComponent<GlitchAI>().inRange)
                    {
                        crosshair.texture = overEnemyTexture;

                        if (Input.GetMouseButtonDown(0))
                        {
							SendMessage ("GetTarget", hit.collider.gameObject);
                            target = hit.transform;
                            FocusOn();
                            DrawCircles();
                            crosshair.gameObject.SetActive(false);
                            crosshair.texture = defaultPointerTexture;

                            abbyAnim.SetBool("EnterFocus", true);
                            Invoke("EnterFocusFalse", 0.5f);
                        }
                    }
                    else
                    {
                        crosshair.texture = defaultPointerTexture;
                    }
                }
                else
                {
                    crosshair.texture = defaultPointerTexture;  
                }
            }
        }
        #endregion

        //Se tiver um alvo, fazer o spotLight e a BattleCam olharem pra ele
        if (target)
        {
            battleCam.transform.LookAt(target.transform.position);
            spot.transform.LookAt(target.transform.position);
        }


        //Tempo de Cooldown do Focus depois de matar ou levar dano
        if (blinking)
        {
            canFocus = false;
            music.GetComponent<AudioChorusFilter>().enabled = !Abby.GetComponent<AbbyLife>().isDead;
            mainCam.GetComponent<MotionBlur>().enabled = true;
            mainCam.GetComponent<NoiseEffect>().enabled = true;
            mainCam.GetComponent<Vignetting>().enabled = true;
            foreach (GameObject enemy in enemyMusic)
            {
                enemy.GetComponent<AudioChorusFilter>().enabled = true;
            }

            blink += Time.deltaTime;

            blinkInterval += Time.deltaTime;

            //PISCAR
            //if (blinkInterval <= 0.25)
            //{
            //    gameObject.renderer.enabled = false;
            //}
            //else if (blinkInterval > 0.25)
            //{
            //    gameObject.renderer.enabled = true;
            //}

            if (blinkInterval >= 0.5)
            {
                blinkInterval = 0;
            }

            if (blink >= blinkTime && GetComponent<AbbyLife>().life > 0)
            {
                blink = 0;
                blinking = false;
                canFocus = true;

                abbyAnim.SetBool("EndDamage", true);
                Invoke("EndDamageFalse", 0.1f);

                music.GetComponent<AudioChorusFilter>().enabled = false;
                mainCam.GetComponent<MotionBlur>().enabled = false;
                mainCam.GetComponent<NoiseEffect>().enabled = false;
                mainCam.GetComponent<Vignetting>().enabled = false;
                foreach (GameObject enemy in enemyMusic)
                {
                    enemy.GetComponent<AudioChorusFilter>().enabled = false;
                }

                blinkInterval = 0;
                //gameObject.renderer.enabled = true;
            }
        }
    }

    void EndDamageFalse()
    {
        abbyAnim.SetBool("EndDamage", false);
    }

    private void FocusOn()
    {
        inFocus = true;
        SendMessage("AutoRotateSwitch", false);
		SendMessage("SetFocusState", true);
        spot.enabled = true;
        directional.enabled = false;
  
        abbyAnim.SetBool("InFocus", true);

        mainCam.enabled = false;
        battleCam.enabled = true;

        music.GetComponent<AudioLowPassFilter>().enabled = true;
        foreach (GameObject enemy in enemyMusic)
        {
            enemy.GetComponent<AudioLowPassFilter>().enabled = true;
        }
		
		GameObject.Find("BattleCamera").GetComponent<DepthOfFieldScatter>().focalTransform = target.transform;

        LockCamera();

        Screen.lockCursor = false;
        return;
    }

    private void FocusOff()
    {
        inFocus = false;
        SendMessage("AutoRotateSwitch", true);
		SendMessage("SetFocusState", false);
        spot.enabled = false;
        directional.enabled = true;

        abbyAnim.SetBool("InFocus", false);

        mainCam.enabled = true;
        battleCam.enabled = false;

        music.GetComponent<AudioLowPassFilter>().enabled = false;
        foreach (GameObject enemy in enemyMusic)
        {
            enemy.GetComponent<AudioLowPassFilter>().enabled = false;
        }

        target = null;

        UnlockCamera();
        EraseCircles();

        Screen.lockCursor = true;

        if (crosshair.enabled == false)
        {
            crosshair.texture = defaultPointerTexture;
        }
        crosshair.gameObject.SetActive(!GetComponent<CinematicControl>().inEvent);
        return;
    }

    private void DrawCircles()
    {
        for (var targetEnergy = target.GetComponent<GlitchAI>().currentEnergy; targetEnergy > 0; targetEnergy -= damBase)
        {
            Instantiate(hardCircle);
        }
    }

    private void EraseCircles()
    {
        aCircles = GameObject.FindGameObjectsWithTag("circle");

        foreach (GameObject circle in aCircles)
        {
            Destroy(circle);
        }
    }

    private void LockCamera()
    {
        GameObject.FindWithTag("MainCamera").GetComponent<MouseOrbit>().xSpeed = 0;
        GameObject.FindWithTag("MainCamera").GetComponent<MouseOrbit>().ySpeed = 0;
    }

    private void UnlockCamera()
    {
        GameObject.FindWithTag("MainCamera").GetComponent<MouseOrbit>().xSpeed = 250;
        GameObject.FindWithTag("MainCamera").GetComponent<MouseOrbit>().ySpeed = 120;
    }

    private void EnterFocusFalse()
    {
        abbyAnim.SetBool("EnterFocus", false);
    }
}