using UnityEngine;
using System.Collections;

public class CamFollow : MonoBehaviour
{
    public GameObject player;
    
    Vector3 initialOffset;

    public float rotDamping = 1;
    public float distDamping = 1;
    private bool dragAround = false;

    private Transform enemyPos;

    void Start()
    {
        initialOffset = player.transform.position - transform.position;
    }

    void LateUpdate()
    {
        Vector3 playerDist = player.transform.position - transform.position;
        if (playerDist.magnitude >= initialOffset.magnitude)
        {
            dragAround = true; //na realidade � true
        }
        else if (playerDist.magnitude < initialOffset.magnitude)
        {
            dragAround = true;
        }

        if (dragAround)
        {
            if (!GameObject.Find("Abby").GetComponent<Focus>().inFocus)
            {
                //Sem Damping - Sem Foco
                float currentAngle = transform.eulerAngles.y;
                float desiredAngle = player.transform.eulerAngles.y;
                float angle = Mathf.LerpAngle(currentAngle, desiredAngle, Time.deltaTime * rotDamping);
                Quaternion rotation = Quaternion.Euler(0, angle, 0);

                transform.position = player.transform.position - (rotation * initialOffset);
            }
            else
            {
                //Sem Damping - Focado
                float currentAngle = transform.eulerAngles.y;
                float desiredAngle = enemyPos.transform.eulerAngles.y;
                float angle = Mathf.LerpAngle(currentAngle, desiredAngle, Time.deltaTime * rotDamping);
                Quaternion rotation = Quaternion.Euler(0, angle, 0);

                transform.position = player.transform.position - (rotation * initialOffset);
            }
        }
        else
        {
            //Com Damping
            float currentAngle = transform.eulerAngles.y;
            float desiredAngle = player.transform.eulerAngles.y;
            float angle = Mathf.LerpAngle(currentAngle, desiredAngle, Time.deltaTime * rotDamping);
            Quaternion rotation = Quaternion.Euler(0, angle, 0);

            Vector3 desiredPosition = player.transform.position - (rotation * initialOffset);
            Vector3 position = Vector3.Lerp(transform.position, desiredPosition, Time.deltaTime * distDamping);
            transform.position = position;
        }

        if (!GameObject.Find("Abby").GetComponent<Focus>().inFocus)
        {
            transform.LookAt(player.transform);
        }
        else
        {
            transform.LookAt(enemyPos);
        }
    }

    public void AssociateEnemy(Transform pos)
    {
        enemyPos = pos;
    }

}