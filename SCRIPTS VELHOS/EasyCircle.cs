﻿using UnityEngine;
using System.Collections;

public class EasyCircle : MonoBehaviour
{
    //private float radius;
    private Transform target;
    private Vector2 randomVec;
    public GameObject[] aCircles;
    public GameObject[] aOuterCircles;

    private float interval = 1;
    private float timer;

    private Vector2 touchDist;
    private float radius;

    void Start()
    {
        radius = GetComponent<GUITexture>().texture.width / 2;
        target = GameObject.FindWithTag("Player").GetComponent<Focus>().target;
    }

    public void Update()
    {
        
    }

    void OnMouseDown()
    {
        target.GetComponent<GlitchAI>().SendMessage("TakeDamage", GameObject.FindWithTag("Player").GetComponent<Focus>().damBase);
        Destroy(gameObject);

        touchDist = new Vector2(Input.mousePosition.x, Input.mousePosition.y) - new Vector2(transform.position.x, transform.position.y);

        if (touchDist.sqrMagnitude < (0.8 * radius) * (0.8 * radius))
        {
            print("GUITexture hit within 80% width circle!");
        }
    }

    void Spawn()
    {
        float randomX = UnityEngine.Random.Range(1, 94);
        randomX *= 0.01f;

        float randomY = UnityEngine.Random.Range(1, 85);
        randomY *= 0.01f;

        randomVec = new Vector2(randomX, randomY);
        transform.position = randomVec;

        aCircles = GameObject.FindGameObjectsWithTag("circle");

        foreach (GameObject circle in aCircles)
        {
            if (gameObject.GetComponent<GUITexture>().HitTest(circle.GetComponent<GUITexture>().transform.position))
            {
                Spawn();
            }
            else
            {
                break;
            }
        }

        return;
    }
}