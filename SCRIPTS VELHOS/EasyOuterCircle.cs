﻿using UnityEngine;
using System.Collections;

public class EasyOuterCircle : MonoBehaviour
{
    private float startScale = 2;
    private Rect initialInset;
    private Rect currentInset;
    private float currentX;
    private float currentY;
    private float shrinkSpeed = 0.5f;

    void Start()
    {
        initialInset = new Rect(50, 50, startScale, startScale);
        currentX = startScale;
        currentY = startScale;
        guiTexture.pixelInset = initialInset;
    }

    void Update()
    {
        currentX -= currentX * shrinkSpeed * Time.deltaTime;
        currentY -= currentY * shrinkSpeed * Time.deltaTime;
        currentInset = new Rect(50, 50, currentX, currentY);
        guiTexture.pixelInset = currentInset;
        print(currentInset);
    }
}